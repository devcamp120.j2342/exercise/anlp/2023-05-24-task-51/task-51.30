import java.util.Stack;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Subtask 1");
        String strSt1a = "bananas";
        String strSt1b = "devcamp";
        System.out.println(removeDuplicates(strSt1a));
        System.out.println(removeDuplicates(strSt1b));

        System.out.println("Subtask 2");
        String strSt2a = "Devcamp java";
        String strSt2b = "Abc  abc bac";
        System.out.println(countWords(strSt2a));
        System.out.println(countWords(strSt2b));

        System.out.println("Subtask 3");
        String strSt3a = "abc";
        String strSt3b = "def";
        String result = strSt3a.concat(strSt3b);
        System.out.println(result);

        System.out.println("Subtask 4");
        String strSt4a = "Devcamp java";
        String strSt4b = "java";
        boolean contains = strSt4a.contains(strSt4b);
        if (contains) {
            System.out.println("s1 contains s2");
        } else {
            System.out.println("s1 does not contain s2");
        }

        System.out.println("Subtask 5");
        String strSt5 = "Devcamp java";
        int k = 3;
        System.out.println(strSt5.charAt(k - 1));

        System.out.println("Subtask 6");
        String strSt6 = "Devcamp java";
        int count = countOccurrences(strSt6, 'a');
        System.out.println(count);

        System.out.println("Subtask 7");
        String strSt7 = "Devcamp java";
        int resultSt7 = strSt7.indexOf('a') + 1;
        System.out.println(resultSt7);

        System.out.println("Subtask 8");
        String strSt8 = "Devcamp";
        System.out.println(strSt8.toUpperCase());

        System.out.println("Subtask 9");
        String strSt9 = "DevCamp";
        System.out.println(swapCase(strSt9));

        System.out.println("Subtask 10");
        String strSt10a = "DevCamp";
        String strSt10b = "DevCamp123";
        System.out.println(countUppercaseCharacters(strSt10a));
        System.out.println(countUppercaseCharacters(strSt10b));

        System.out.println("Subtask 11");
        String strSt11a = "DevCamp123";
        String strSt11b = "DEVCAMP123";
        displayCharactersInRange(strSt11a, 'A', 'Z');
        displayCharactersInRange(strSt11b, 'A', 'Z');

        System.out.println("Subtask 12");
        String strSt12 = "abcdefghijklmnopqrstuvwxy";
        divideString(strSt12, 5);
        
        System.out.println("Subtask 13");
        String strSt13 = "aabaarbarccrabmq";
        System.out.println(removeConsecutiveDuplicates(strSt13));

        System.out.println("Subtask 14");
        String strSt14a = "Welcome";
        String strSt14b = "home";
        String resultSt14 = concatenateStrings(strSt14a, strSt14b);
        System.out.println(resultSt14);
    

        System.out.println("Subtask 15");
        String strSt15 = "DevCamp";
        System.out.println(swapCase(strSt15));

        System.out.println("Subtask 16");
        String strSt16a = "DevCamp";
        String strSt16b = "DevCamp123";
        System.out.println(containsNumber(strSt16a));
        System.out.println(containsNumber(strSt16b));

        System.out.println("Subtask 17");
        String strSt17a = "DevCamp123";
        String strSt17b = "DEVCAMP 123";
        System.out.println(isValidFormat(strSt17a));
        System.out.println(isValidFormat(strSt17b));
    }

    public static String removeDuplicates(String str) {
        StringBuilder sb = new StringBuilder();
        int[] count = new int[256]; // Mảng để đếm số lần xuất hiện của các ký tự ASCII

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (count[c] == 0) {
                sb.append(c); // Nếu ký tự chưa xuất hiện, thêm vào kết quả
            }
            count[c]++; // Tăng số lần xuất hiện của ký tự
        }

        return sb.toString();
    }

    public static int countWords(String str) {
        String[] words = str.trim().split("\\s+");
        return words.length;
    }

    public static int countOccurrences(String str, char target) {
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == target) {
                count++;
            }
        }

        return count;
    }

    public static String swapCase(String str) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (Character.isUpperCase(c)) {
                result.append(Character.toLowerCase(c));
            } else if (Character.isLowerCase(c)) {
                result.append(Character.toUpperCase(c));
            } else {
                result.append(c);
            }
        }

        return result.toString();
    }

    public static int countUppercaseCharacters(String str) {
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (Character.isUpperCase(c)) {
                count++;
            }
        }

        return count;
    }

    public static void divideString(String str, int n) {
        if (str.length() % n != 0) {
            System.out.println("KO");
            return;
        }

        int numOfParts = str.length() / n;

        for (int i = 0; i < numOfParts; i++) {
            String part = str.substring(i * n, (i + 1) * n);
            System.out.println(part);
        }
    }

    public static void displayCharactersInRange(String str, char startChar, char endChar) {
        StringBuilder result = new StringBuilder();

        for (char c = startChar; c <= endChar; c++) {
            if (str.indexOf(c) != -1) {
                result.append(c); // Nếu ký tự chưa xuất hiện, thêm vào kết quả
            }
        }
        System.out.println(result.toString());
    }

        public static String removeConsecutiveDuplicates(String str) {
            Stack<Character> stack = new Stack<>();
            
            for (int i = 0; i < str.length(); i++) {
                char currentChar = str.charAt(i);
    
                // Kiểm tra xem ngăn xếp có rỗng không và ký tự hiện tại giống ký tự đầu tiên trong ngăn xếp
                if (!stack.isEmpty() && currentChar == stack.peek()) {
                    stack.pop();  // Xoá ký tự trong ngăn xếp nếu giống nhau
                } else {
                    stack.push(currentChar);  // Thêm ký tự vào ngăn xếp nếu khác nhau
                }
            }
    
            StringBuilder result = new StringBuilder();
            
            // Xây dựng chuỗi kết quả từ ngăn xếp
            while (!stack.isEmpty()) {
                result.insert(0, stack.pop());  // Thêm ký tự vào đầu chuỗi kết quả
            }
            
            return result.toString();
        }



    public static String concatenateStrings(String str1, String str2) {
        int len1 = str1.length();
        int len2 = str2.length();
        
        if (len1 > len2) {
            str1 = str1.substring(len1 - len2);
        } else if (len1 < len2) {
            str2 = str2.substring(len2 - len1);
        }

        return str1 + str2;
    }

    public static boolean containsNumber(String str) {
        String regex = ".*\\d.*";
        return str.matches(regex);
    }


public static boolean isValidFormat(String str) {
    String regex = "^[A-Z][a-zA-Z0-9]{0,18}[0-9]$";
    return str.matches(regex);
}
}